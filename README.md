# Kata template for Kotlin using IntelliJ and Gradle

## Setup Instructions

Facilitators should fork this template project to create a **Kotlin** starter project which uses **IntelliJ** and **Gradle** to develop a solution for a given kata.

First, ensure that a sub-group https://gitlab.com/boston-software-crafters/kata-starter-projects/kata-name exists where *kata-name* is the name of the kata being set up, creating it as necessary.

Next, ensure that a sub-group https://gitlab.com/boston-software-crafters/kata-starter-projects/kata-name/kotlin exists, creating it as necessary.

Then fork the new project to https://gitlab.com/boston-software-crafters/kata-starter-projects/kata-name/kotlin/ and tailor the forked starter files as necessary (depends on the kata).

After forking, replace the content of this file (**README.md**) with the a short description of the other *.md files.
